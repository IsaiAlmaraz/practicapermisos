package iab.noxiandev.practicapermisos;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText tel = (EditText) findViewById(R.id.editTelefono);
        ImageButton btnTel = (ImageButton) findViewById(R.id.btnTelefono);
        btnTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = tel.getText().toString();
                if(num != null){
                    // -- Comprobar versión actual de android
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        versionNueva();
                    }else {
                        versionesAnteriores(num);
                    }
                }
            }
        });
    }
    public void versionNueva(){

    }
    // Versión de teléfono
    public void versionesAnteriores(String num){
        Intent iLlamada = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+num));
        if (verificaPermisos(Manifest.permission.CALL_PHONE)){
            startActivity(iLlamada);
        }else{
            Toast.makeText(MainActivity.this, "Configura los permisos", Toast.LENGTH_SHORT);
        }
    }

    // Comprobar si esta en el manifest
    private boolean verificaPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return  resultado == PackageManager.PERMISSION_GRANTED;
    }


}